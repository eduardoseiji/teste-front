<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="css/style.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Prova Front Webart</title>
</head>
<body>
    <header class="header">
        <div class="row align-center">
            <div class="boxLogoHeader columns small-12 flex-container align-center">
                <img src="img/css_img.png" alt="CSS 3" class="btnLogo">
            </div>
            <div class="columns small-12 show-for-medium">
                <nav>
                    <ul class="menu-header flex-container align-justify">
                        <li >Menu 01</li>
                        <li>Menu 02</li>
                        <li>Menu 03</li>
                        <li>Menu 04</li>
                        <li>Menu 05</li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <main class="main">
        <section class="section-imagens">
            <div class="row">
                <div class="columns small-12 medium-5">
                    <div class="banner-1 banner-img"></div>
                </div>
                <div class="columns small-12 medium-7">
                    <div class="banner-2 banner-img degrade">

                    </div>
                </div>
            </div>
        </section>
        <section class="section-texto">
            <div class="row">
                <div class="columns small-12">
                    <div class="content-texto">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque dolorem fugiat sunt ullam! Magnam at harum praesentium, tenetur alias impedit, <span class="destaque-strong"> in voluptatibus eius molestiae error tempora autem. </span> Quam deserunt, nisi. Molestias pariatur aut beatae distinctio reiciendis nemo officia, et, excepturi praesentium exercitationem culpa totam <span class="destaque-danger">possimus nostrum velit</span>, maiores dignissimos id commodi iure laboriosam labore autem ipsam saepe. Tempore est ad corrupti <span class="destaque-strong"> similique culpa autem vero officiis!</span> Ad at fugiat exercitationem <span class="destaque-alert">minima provident voluptates numquam</span>, ex odit facere eos autem debitis tempora nam. Officia ipsam obcaecati qui hic nostrum quisquam, cum.</p>
                    </div>
                </div>
            </div>
        </section>
        <div class="section-infos">
            <div class="row">
                <div class="wrapTabela columns small-12 medium-5">
                    <div class="tabela">
                        <table>
                            <thead>
                                <tr>
                                    <th>Mês</th>
                                    <th>% cancelado</th>
                                    <th>Visitas</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>02/2018</td>
                                    <td>14%</td>
                                    <td>29670</td>
                                </tr>
                                <tr>
                                    <td>02/2018</td>
                                    <td>14%</td>
                                    <td>29670</td>
                                </tr>
                                <tr>
                                    <td>02/2018</td>
                                    <td>14%</td>
                                    <td>29670</td>
                                </tr>
                                <tr>
                                    <td>02/2018</td>
                                    <td>14%</td>
                                    <td>29670</td>
                                </tr>
                                <tr>
                                    <td>02/2018</td>
                                    <td>14%</td>
                                    <td>29670</td>
                                </tr>
                                <tr>
                                    <td>02/2018</td>
                                    <td>14%</td>
                                    <td>29670</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="wrapToggle .columns small-12 medium-offset-2 medium-5">
                    <h2  data-toggle="#imagem-js">Toggle</h2>
                    <img src="img/js_img.png" alt="Javascript toggle" id="imagem-js">
                </div>
            </div>
        </div>
    </main>
    <footer class="footer flex-container align-center align-middle">
        <div class="row">
            <div class="columns small-12">
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
            </div>
        </div>
    </footer>
    <div class="wrapPopUp">
        <div class="backgroundPopUp"></div>
        <div class="popUpContent">
            <div class="row flex-container align-center align-middle">
                <div class="columns small-12 medium-10">
                    <div class="modalContent flex-container flex-dir-column">
                        <span class="btnCloseModal">X</span>
                        <img src="img/css_img.png" alt="CSS3" class="imgModal">
                        <h2 class="titleModal">CSS</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime dolore sed, quia inventore, nesciunt quis quaerat veritatis amet, quod officia voluptatum. Vero hic laboriosam dolor ratione commodi minus accusamus. Magni, animi ducimus doloremque at iste tempore molestiae. Totam optio quam eligendi corporis itaque expedita, tempora neque odio perspiciatis temporibus. Ullam incidunt error, consequuntur eos ex velit et, voluptates natus dolore, necessitatibus odit ea iste repudiandae vero. Nostrum inventore fuga quibusdam cum? Cum cumque doloribus commodi qui, facere id voluptas aliquam obcaecati maxime et ea a, totam quod provident fugiat eum rem ex, quis accusantium debitis, itaque eos quos reiciendis delectus.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime dolore sed, quia inventore, nesciunt quis quaerat veritatis amet, quod officia voluptatum. Vero hic laboriosam dolor ratione commodi minus accusamus. Magni, animi ducimus doloremque at iste tempore molestiae. Totam optio quam eligendi corporis itaque expedita, tempora neque odio perspiciatis temporibus. Ullam incidunt error, consequuntur eos ex velit et, voluptates natus dolore, necessitatibus odit ea iste repudiandae vero. Nostrum inventore fuga quibusdam cum? Cum cumque doloribus commodi qui, facere id voluptas aliquam obcaecati maxime et ea a, totam quod provident fugiat eum rem ex, quis accusantium debitis, itaque eos quos reiciendis delectus.Nostrum inventore fuga quibusdam cum? Cum cumque doloribus commodi qui, facere id voluptas aliquam obcaecati maxime et ea a, totam quod provident fugiat eum rem ex, quis accusantium debitis, itaque eos quos reiciendis delectus.Nostrum inventore fuga quibusdam cum? Cum cumque doloribus commodi qui, facere id voluptas aliquam obcaecati maxime et ea a, totam quod provident fugiat eum rem ex, quis accusantium debitis, itaque eos quos reiciendis delectus.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="js/main.js"></script>
</body>
</html>