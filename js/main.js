"use strict";
{
    const botaoToggle = document.querySelector('[data-toggle]');
    
    botaoToggle.addEventListener('click', toggleJS);

    function toggleJS(e) {
        const target = e.target.dataset.toggle;
        
        const img = document.querySelector(target);        
        
        img.classList.toggle('hide');
        
    }


    const botaoCSS          = document.querySelector('.btnLogo');
    const body              = document.querySelector('body');
    const btnCloseModal     = document.querySelector('.btnCloseModal');
    const backgroundModal   = document.querySelector('.backgroundPopUp');

    botaoCSS.addEventListener('click', openModal);
    backgroundModal.addEventListener('click', closeModal);
    btnCloseModal.addEventListener('click', closeModal);

    function openModal() {
        body.classList.add('open-modal');
    }

    function closeModal() {
        body.classList.remove('open-modal');
    }

    
}